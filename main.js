var key = new Buffer('8CBDEC62EB4DCA778F842B02503011B2', 'hex')
var src = new Buffer('0002123401010100000000000000c631', 'hex')
// ruleid:rules_lgpl_javascript_crypto_rule-node-aes-ecb
cipher = crypto.createCipheriv("aes-128-ecb", key, iv)
